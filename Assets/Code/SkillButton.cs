using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SkillButton : MonoBehaviour
{
    public KeyCode key;
    public int but_num;
    public List<GameObject> pets_obj;
    public Image timer_bar_img;
    public TMP_Text t_name;

    public GameObject curr_pet;

    public float cd_time;
    private float curr_time;

    bool skill_access = false;
    bool skill_used = false;


    public void _Start()
    {
        skill_access = false;
        skill_used = false;
        curr_time = 0;
        timer_bar_img.enabled = true;

        foreach (GameObject gm in pets_obj)
        {
            gm.SetActive(false);
        }

        switch (but_num)
        {
            case 1:
                pets_obj[PlayerPrefs.GetInt("pet_slot_1")].SetActive(true);
                break;
            case 2:
                pets_obj[PlayerPrefs.GetInt("pet_slot_2")].SetActive(true);
                break;
            case 3:
                pets_obj[PlayerPrefs.GetInt("pet_slot_3")].SetActive(true);
                break;
            case 4:
                pets_obj[PlayerPrefs.GetInt("pet_slot_4")].SetActive(true);
                break;
            case 5:
                pets_obj[PlayerPrefs.GetInt("pet_slot_5")].SetActive(true);
                break;
            case 6:
                pets_obj[PlayerPrefs.GetInt("pet_slot_6")].SetActive(true);
                break;
        }

        t_name.text = "" + curr_pet.GetComponent<PetBrain>().pet_name;

        StartCoroutine(Timer());
    }

    private void Update()
    {
        if (Input.GetKeyDown(key))
        {
            But_Skill();
        }
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(0);
        curr_time += Time.deltaTime;
        timer_bar_img.fillAmount = curr_time / cd_time;

        if (curr_time < 7)
        {
            StartCoroutine(Timer());
        } 
        else
        {
            skill_access = true;
        }
    }

    public void But_Skill()
    {
        if (skill_access && !skill_used)
        {
            curr_pet.GetComponent<PetBrain>().ulta = true;
            timer_bar_img.enabled = false;
            skill_used = true;
        }
    }
}
