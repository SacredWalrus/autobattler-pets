using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MousePosition : MonoBehaviour
{
    public GameObject target;
    public GameObject selection_circle;
    public LayerMask layer_ground, layer_enemy;
    public Camera cam;

    public bool visual_accept = false;

    private void Update()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, float.MaxValue, layer_ground))
        {
            transform.position = new Vector3(hit.point.x,
                                             0.186f,
                                             hit.point.z);
        }

        if (visual_accept)
        {
            selection_circle.SetActive(true);

            Ray ray1 = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit1;

            if (Physics.Raycast(ray1, out hit1, float.MaxValue, layer_enemy))
            {
                target = hit1.collider.gameObject;
            }
            else target = null;
        } 
        else selection_circle.SetActive(false);        
    }
}
