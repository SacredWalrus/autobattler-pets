using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FightController : MonoBehaviour
{
    public List<GameObject> pets_in_arena;
    public List<Transform> random_point_in_surface;

    public float round_time = 30;
    public float curr_round_time;
    public int round_num = 0;

    public int team_1_kill, team_2_kill;
    public int team_1_points, team_2_points;

    public Image team_1_point_1, team_1_point_2, team_1_point_3;
    public Image team_2_point_1, team_2_point_2, team_2_point_3;

    public TMP_Text t_timer, t_team_1_points, t_team_2_points;

    public List<GameObject> pets;
    public List<string> team_2_name;

    public List<Transform> team_1_spawn_pos;
    public List<Transform> team_2_spawn_pos;

    public Image round_img;
    public Sprite spr_round_1, spr_round_2, spr_round_3;

    public List<GameObject> img_skills;

    public float min_border_x, max_border_x;
    public float min_border_z, max_border_z;


    private void Start()
    {
        round_img.enabled = false;

        team_1_points = 0;
        team_2_points = 0;

        StartRound();
    }

    private void Update()
    {
        t_timer.text = (int)curr_round_time + "";
        t_team_1_points.text = team_1_kill + "";
        t_team_2_points.text = team_2_kill + "";
    }

    void StartRound()
    {
        round_num++;

        team_1_kill = 0;
        team_2_kill = 0;

        if (round_num == 1)
        {
            GameObject gm11 = Instantiate(pets[PlayerPrefs.GetInt("pet_slot_1")], team_1_spawn_pos[0].position, transform.rotation);
            GameObject gm12 = Instantiate(pets[PlayerPrefs.GetInt("pet_slot_2")], team_1_spawn_pos[1].position, transform.rotation);
            //GameObject gm13 = Instantiate(pets[PlayerPrefs.GetInt("pet_slot_3")], team_1_spawn_pos[2].position, transform.rotation);

            GameObject gm1 = Instantiate(pets[PlayerPrefs.GetInt("pet_slot_4")], team_2_spawn_pos[0].position, transform.rotation);
            GameObject gm2 = Instantiate(pets[PlayerPrefs.GetInt("pet_slot_5")], team_2_spawn_pos[1].position, transform.rotation);
            //GameObject gm3 = Instantiate(pets[PlayerPrefs.GetInt("pet_slot_6")], team_2_spawn_pos[2].position, transform.rotation);

            gm1.GetComponent<PetBrain>().team = PetBrain.Team.team_2;
            gm2.GetComponent<PetBrain>().team = PetBrain.Team.team_2;
            //gm3.GetComponent<PetBrain>().team = PetBrain.Team.team_2;

            pets_in_arena.Add(gm11);
            pets_in_arena.Add(gm12);
            //pets_in_arena.Add(gm13);
            pets_in_arena.Add(gm1);
            pets_in_arena.Add(gm2);
            //pets_in_arena.Add(gm3);

            img_skills[0].GetComponent<SkillButton>().curr_pet = gm11;
            img_skills[1].GetComponent<SkillButton>().curr_pet = gm12;
            //img_skills[2].GetComponent<SkillButton>().curr_pet = gm13;

            img_skills[2].GetComponent<SkillButton>().curr_pet = gm1;
            img_skills[3].GetComponent<SkillButton>().curr_pet = gm2;
            //img_skills[5].GetComponent<SkillButton>().curr_pet = gm3;
        }

        foreach (GameObject gm in img_skills)
        {
            gm.GetComponent<SkillButton>()._Start();
        }          

        curr_round_time = round_time;

        StartCoroutine(RoundTimer());
        StartCoroutine(RoundImage());
    }

    IEnumerator RoundImage()
    {
        round_img.enabled = true;
        switch (round_num)
        {
            case 1:
                round_img.sprite = spr_round_1;
                break;
            case 2:
                round_img.sprite = spr_round_2;
                break;
            case 3:
                round_img.sprite = spr_round_3;
                break;
        }
        yield return new WaitForSeconds(1.5f);
        foreach (GameObject gm in pets_in_arena)
        {
            gm.GetComponent<PetBrain>().round_start = true;
        }
        round_img.enabled = false;
    }

    IEnumerator RoundTimer()
    {
        yield return new WaitForSeconds(0);
        curr_round_time -= Time.deltaTime;

        if (curr_round_time < 0)
        {
            curr_round_time = 0;
            Respawn();
            RoundResults();
            StartRound();
        }
        else
        {
            StartCoroutine(RoundTimer());
        }
    }

    void Respawn()
    {
        foreach (GameObject gm in pets_in_arena)
        {
            gm.GetComponent<PetBrain>().Respawn();
            gm.GetComponent<PetBrain>().round_start = true;
        }
    }

    void RoundResults()
    {
        int curr_point = 0;

        if (team_1_kill > team_2_kill)
        {
            team_1_points++;
            curr_point = 1;
        }

        if (team_2_kill > team_1_kill)
        {
            team_2_points++;
            curr_point = 2;
        }

        if (team_1_kill == team_2_kill)
        {
            team_1_points++;
            team_2_points++;
            curr_point = 3;
        }

        switch (round_num)
        {
            case 1:
                if (curr_point == 1)
                {
                    team_1_point_1.color = new Color(0.137451f, 0.6226415f, 0.1897976f);
                    team_2_point_1.color = new Color(0.7264151f, 0f, 0f);
                }

                if (curr_point == 2)
                {
                    team_2_point_1.color = new Color(0.137451f, 0.6226415f, 0.1897976f);
                    team_1_point_1.color = new Color(0.7264151f, 0f, 0f);
                }

                if (curr_point == 3)
                {
                    team_2_point_1.color = new Color(0.137451f, 0.6226415f, 0.1897976f);
                    team_1_point_1.color = new Color(0.137451f, 0.6226415f, 0.1897976f);
                }
                break;

            case 2:
                if (curr_point == 1)
                {
                    team_1_point_2.color = new Color(0.137451f, 0.6226415f, 0.1897976f);
                    team_2_point_2.color = new Color(0.7264151f, 0f, 0f);
                }

                if (curr_point == 2)
                {
                    team_2_point_2.color = new Color(0.137451f, 0.6226415f, 0.1897976f);
                    team_1_point_2.color = new Color(0.7264151f, 0f, 0f);
                }

                if (curr_point == 3)
                {
                    team_2_point_2.color = new Color(0.137451f, 0.6226415f, 0.1897976f);
                    team_1_point_2.color = new Color(0.137451f, 0.6226415f, 0.1897976f);
                }
                break;

            case 3:
                if (curr_point == 1)
                {
                    team_1_point_3.color = new Color(0.137451f, 0.6226415f, 0.1897976f);
                    team_2_point_3.color = new Color(0.7264151f, 0f, 0f);
                }

                if (curr_point == 2)
                {
                    team_2_point_3.color = new Color(0.137451f, 0.6226415f, 0.1897976f);
                    team_1_point_3.color = new Color(0.7264151f, 0f, 0f);
                }

                if (curr_point == 3)
                {
                    team_2_point_3.color = new Color(0.137451f, 0.6226415f, 0.1897976f);
                    team_1_point_3.color = new Color(0.137451f, 0.6226415f, 0.1897976f);
                }
                break;
        }
    }

    public Vector3 GenerateRandomPoint(float max_dist_to_target, Vector3 this_pos)
    {
        bool get_correct_point = false;
        int num = 0;

        while (!get_correct_point)
        {
            num = Random.Range(0, random_point_in_surface.Count);

            if (Vector3.Distance(random_point_in_surface[num].position, this_pos) <= max_dist_to_target)
            {
                get_correct_point = true;
            }
        }

        return random_point_in_surface[num].position;
    }

    public Vector3 GenerateRandomPointDistToEnemy(float max_dist_to_target, Vector3 enemy_pos)
    {
        bool get_correct_point = false;
        int num = 0;

        while (!get_correct_point)
        {
            num = Random.Range(0, random_point_in_surface.Count);

            if (Vector3.Distance(random_point_in_surface[num].position, enemy_pos) >= max_dist_to_target)
            {
                get_correct_point = true;
            }
        }

        return random_point_in_surface[num].position;
    }

    public void But_Back()
    {
        Application.LoadLevel("Menu");
    }
}
