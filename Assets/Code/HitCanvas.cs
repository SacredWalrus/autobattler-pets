using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HitCanvas : MonoBehaviour
{
    public float hit_dmg;
    public TMP_Text t_hit;
    public float speed;

    private float color_a = 1;

    private void Start()
    {
        t_hit.text = hit_dmg + "";

        transform.eulerAngles = new Vector3(34, 0, 0);

        Destroy(gameObject, 3);
    }

    private void Update()
    {
        transform.Translate(Vector3.up * speed * Time.deltaTime);
        t_hit.color = new Color(t_hit.color.r, t_hit.color.g, t_hit.color.b, color_a -= Time.deltaTime);
    }
}
