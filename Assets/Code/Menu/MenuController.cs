using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    public GameObject active_pick_img;
    public List<string> pets_name;

    public List<int> save_pet;

    public void CardTap(int num)
    {
        active_pick_img.GetComponent<PickController>().Pick(num, pets_name[num]);
    }

    public void But_Fight()
    {
        PlayerPrefs.SetInt("pet_slot_1", save_pet[0]);
        PlayerPrefs.SetInt("pet_slot_2", save_pet[1]);
        //PlayerPrefs.SetInt("pet_slot_3", save_pet[2]);
        PlayerPrefs.SetInt("pet_slot_4", save_pet[3]);
        PlayerPrefs.SetInt("pet_slot_5", save_pet[4]);
        //PlayerPrefs.SetInt("pet_slot_6", save_pet[5]);
        Application.LoadLevel("NewArena");
    }
}
