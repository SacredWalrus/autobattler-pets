using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PickController : MonoBehaviour
{
    public int place_num;

    public MenuController menu;
    public List<GameObject> pick_imgs;
    public List<GameObject> pets_obj;
    public TMP_Text t_name;

    public Image img_outline;

    public bool not_active = true;

    private void Start()
    {
        foreach (GameObject gm in pets_obj)
        {
            gm.SetActive(false);
        }

        t_name.text = "";

        img_outline.enabled = false;
    }

    public void But_Tap()
    {
        if (not_active)
        {
            foreach (GameObject gm in pick_imgs)
            {
                gm.GetComponent<PickController>().img_outline.enabled = false;
                gm.GetComponent<PickController>().not_active = true;
            }

            img_outline.enabled = true;
            menu.active_pick_img = gameObject;
            not_active = false;

            return;
        }

        if (!not_active)
        {
            img_outline.enabled = false;
            menu.active_pick_img = null;
        }
    }

    public void Pick(int num_pet, string name_pet)
    {
        foreach (GameObject gm in pets_obj)
        {
            gm.SetActive(false);
        }

        pets_obj[num_pet].SetActive(true);
        t_name.text = name_pet;

        menu.save_pet[place_num] = num_pet;
    }
}
