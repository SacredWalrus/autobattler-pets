using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using TMPro;
using UnityEditor;

public class PetBrain : MonoBehaviour
{
    [Header("��������� ��� ���������")]
    public float dmg;
    public float hp;
    public float speed;

    [Header("��������� ��� ������")]
    public GameObject canvas_hit;
    public enum Team
    {
        team_1,
        team_2
    }
    public Team team;

    public GameObject canvas_ui;

    public SkinnedMeshRenderer body, head;

    public float rotation_speed;

    public Image bar_back, bar_fill;
    public TMP_Text t_name;

    public Material hit_mat_1, hit_mat_2;
    public Material head_mat, body_mat;

    //Private Paraments
    //[HideInInspector]
    public float curr_hp;

    [HideInInspector]
    public bool round_start = false;

    [HideInInspector]
    public string pet_name;

    [HideInInspector]
    public Rigidbody rb;

    //[HideInInspector]
    public GameObject target;    

    [HideInInspector]
    public bool die = false;

    [HideInInspector]
    public bool ulta = false;    

    [HideInInspector]
    public bool stan;  //�� ������� ���� � �����

    private Vector3 start_pos;

    public bool selection_active;



    private void Start()
    {
        //agent = GetComponent<NavMeshAgent>();
        rb = GetComponent<Rigidbody>();

        selection_active = false;

        if (team == Team.team_1)
        {
            bar_back.color = new Color(0.08807548f, 0.389f, 0.1198805f);
            bar_fill.color = new Color(0.137451f, 0.6226415f, 0.1897976f);
            t_name.color = new Color(0.137451f, 0.6226415f, 0.1897976f);
        }

        if (team == Team.team_2)
        {
            bar_back.color = new Color(0.554f, 0.2450385f, 0f);
            bar_fill.color = new Color(0.9433962f, 0.419228f, 0f);
            t_name.color = new Color(0.9433962f, 0.419228f, 0f);
        }

        t_name.text = pet_name;
        curr_hp = hp;
        start_pos = transform.position;
    }

    private void Update()
    {
        if (target == null || target.GetComponent<PetBrain>().die)
        {
            FindTarget();
        }

        bar_fill.fillAmount = curr_hp / hp;
        //canvas_ui.transform.LookAt(Camera.main.transform);
        canvas_ui.transform.eulerAngles = new Vector3(-30, -180, 0);

        if (curr_hp > hp) curr_hp = hp;
    }

    public void Movement(Vector3 target_pos)
    {
        //agent.SetDestination(target_pos);
        rb.velocity += transform.forward * speed * Time.deltaTime;

        //LookAt
        Vector3 lookPos = target_pos - transform.position;
        lookPos.y = 0;
        Quaternion rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotation_speed);
    }

    public void MovementBack(Vector3 target_pos)
    {
        //agent.SetDestination(target_pos);
        rb.velocity -= transform.forward * speed * Time.deltaTime;

        //LookAt
        Vector3 lookPos = target_pos - transform.position;
        lookPos.y = 0;
        Quaternion rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotation_speed);
    }

    public void FindTarget()
    {
        selection_active = false;
        GameObject tg = null;
        int counter = 0;

        while (tg == null)
        {
            counter++;

            tg = GameObject.Find("GameController").GetComponent<FightController>().pets_in_arena[Random.Range(0, GameObject.Find("GameController").GetComponent<FightController>().pets_in_arena.Count)];

            if (tg.GetComponent<PetBrain>().team != team)
            {
                target = tg;
            }
            else tg = null;

            if (counter >= 10) return;
        }
    }

    public float DistToTarget(Vector3 target_pos)
    {
        return Vector3.Distance(transform.position, target_pos);
    }

    public Vector3 GenerateRandomPoint()
    {
        Vector3 target_pos = transform.forward - transform.forward * Random.Range(1f, 4f);
        target_pos += transform.right * Random.Range(-4f, 4f);

        return target_pos;
    }

    public IEnumerator HitEnum()
    {
        //On 1
        body.material = hit_mat_1;
        head.material = hit_mat_1;

        yield return new WaitForSeconds(0.05f);

        //Off
        body.material = body_mat;
        head.material = head_mat;

        yield return new WaitForSeconds(0.05f);

        //On 2
        body.material = hit_mat_2;
        head.material = hit_mat_2;

        yield return new WaitForSeconds(0.05f);

        body.material = body_mat;
        head.material = head_mat;
    }

    public void Hit(GameObject new_target, float damage)
    {
        GameObject canv = Instantiate(canvas_hit, new Vector3(Random.Range(transform.position.x - 0.3f, transform.position.x + 0.3f), transform.position.y + 0.65f, transform.position.z - 0.66f), transform.rotation);
        canv.GetComponent<HitCanvas>().hit_dmg = damage;

        if (DistToTarget(new_target.transform.position) < 3 && !selection_active)
            target = new_target;
    }

    public void Respawn()
    {
        stan = false;
        die = false;
        ulta = false;
        curr_hp = hp;
        transform.position = new Vector3(start_pos.x, 0.5155487f, start_pos.z);        
    }
}
