using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HillerShield : MonoBehaviour
{
    public PetBrain brain;


    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Enemy" && other.gameObject.GetComponentInParent<PetBrain>().team == brain.team)
        {
            other.gameObject.GetComponentInParent<PetBrain>().curr_hp += Time.deltaTime * 3;
        }
    }
}
