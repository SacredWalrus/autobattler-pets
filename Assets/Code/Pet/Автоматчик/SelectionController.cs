using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionController : MonoBehaviour
{
    public enum Pet
    {
        Uzi,
        Knife,
        Zemelya,
        Grenade,
        Fireman,
        Hiller
    }
    public Pet curr_pet;

    public GameObject father;
    public PetBrain brain;

    MousePosition mouse;
    bool tap = false;

    public LineRenderer line;
    public GameObject selection_circle;

    private void Start()
    {
        tap = false;

        line.enabled = false;
        selection_circle.SetActive(false);
    }

    private void OnMouseDown()
    {
        tap = true;
        mouse.visual_accept = true;
        line.enabled = true;

        selection_circle.SetActive(true);
    }

    private void OnMouseUp()
    {
        tap = false;

        mouse.visual_accept = false;
        line.enabled = false;
        selection_circle.SetActive(false);

        if (mouse.target != null && mouse.target != gameObject)
        {
            brain.target = mouse.target.GetComponent<SelectionController>().father;
            brain.selection_active = true;

            switch (curr_pet)
            {
                case Pet.Uzi:
                    //father.GetComponent<PetUziController>().attack_access = true;
                    //father.GetComponent<PetUziController>().curr_clip = father.GetComponent<PetUziController>().max_clip;
                    father.GetComponent<PetUziController>().curr_state = PetUziController.State.none;                    
                    break;
                case Pet.Knife:
                    father.GetComponent<PetKnifeController>().curr_state = PetKnifeController.State.none;
                    break;
                case Pet.Zemelya:
                    father.GetComponent<PetStoneController>().curr_state = PetStoneController.State.none;
                    break;
                case Pet.Grenade:
                    father.GetComponent<PetGranadeController>().curr_state = PetGranadeController.State.none;
                    break;
                case Pet.Fireman:
                    father.GetComponent<PetFiremanController>().curr_state = PetFiremanController.State.none;
                    break;
                case Pet.Hiller:
                    father.GetComponent<PetHillerController>().curr_state = PetHillerController.State.none;
                    break;
            }

            Debug.Log("ChangeTarget");
        }
        else
        {
            switch (curr_pet)
            {
                case Pet.Uzi:
                    father.GetComponent<PetUziController>().StopAction();
                    //father.GetComponent<PetUziController>().AttackOff();
                    father.GetComponent<PetUziController>().selection_point = new Vector3(mouse.transform.position.x, father.transform.position.y, mouse.transform.position.z);
                    father.GetComponent<PetUziController>().curr_state = PetUziController.State.WalkToSelection;
                    
                    break;
                case Pet.Knife:
                    father.GetComponent<PetKnifeController>().StopAction();
                    father.GetComponent<PetKnifeController>().selection_point = new Vector3(mouse.transform.position.x, father.transform.position.y, mouse.transform.position.z);
                    father.GetComponent<PetKnifeController>().curr_state = PetKnifeController.State.WalkToSelection;
                    break;
                case Pet.Zemelya:
                    father.GetComponent<PetStoneController>().StopAction();
                    father.GetComponent<PetStoneController>().selection_point = new Vector3(mouse.transform.position.x, father.transform.position.y, mouse.transform.position.z);
                    father.GetComponent<PetStoneController>().curr_state = PetStoneController.State.WalkToSelection;
                    break;
                case Pet.Grenade:
                    father.GetComponent<PetGranadeController>().StopAction();
                    father.GetComponent<PetGranadeController>().selection_point = new Vector3(mouse.transform.position.x, father.transform.position.y, mouse.transform.position.z);
                    father.GetComponent<PetGranadeController>().curr_state = PetGranadeController.State.WalkToSelection;
                    break;
                case Pet.Fireman:
                    father.GetComponent<PetFiremanController>().StopAction();
                    father.GetComponent<PetFiremanController>().selection_point = new Vector3(mouse.transform.position.x, father.transform.position.y, mouse.transform.position.z);
                    father.GetComponent<PetFiremanController>().curr_state = PetFiremanController.State.WalkToSelection;
                    break;
                case Pet.Hiller:
                    father.GetComponent<PetHillerController>().StopAction();
                    father.GetComponent<PetHillerController>().selection_point = new Vector3(mouse.transform.position.x, father.transform.position.y, mouse.transform.position.z);
                    father.GetComponent<PetHillerController>().curr_state = PetHillerController.State.WalkToSelection;
                    break;
            }
        }
    }

    private void Update()
    {
        mouse = GameObject.Find("mouse").GetComponent<MousePosition>();

        if (tap)
        {
            line.SetPosition(0, new Vector3(transform.position.x, 0.2f, transform.position.z));
            line.SetPosition(1, new Vector3(mouse.transform.position.x, 0.2f, mouse.transform.position.z));
        }
    }
}
