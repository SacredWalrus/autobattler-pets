using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UziBulletController : MonoBehaviour
{
    public PetBrain brain;
    public GameObject father;
    public GameObject target;
    public float speed;

    private void Update()
    {
        //LookAt
        Vector3 lookPos = target.transform.position - transform.position;
        //lookPos.y = 0;
        Quaternion rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 1);

        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "shield")
        {
            Destroy(gameObject);
            return;
        }

        if (other.tag == "Enemy" && other.gameObject.GetComponentInParent<PetBrain>().team != brain.team)
        {
            other.gameObject.GetComponentInParent<PetBrain>().StartCoroutine(other.gameObject.GetComponentInParent<PetBrain>().HitEnum());
            other.gameObject.GetComponentInParent<PetBrain>().Hit(father, brain.dmg);
            other.gameObject.GetComponentInParent<PetBrain>().curr_hp -= brain.dmg;
        }

        Destroy(gameObject);
    }
}
