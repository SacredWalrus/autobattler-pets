using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtomBomb : MonoBehaviour
{
    public float speed;

    public PetBrain brain;
    public GameObject father;

    SphereCollider boom_col;
    public ParticleSystem boom_vfx;

    bool boom_activate;

    public GameObject mesh;

    private void Start()
    {
        boom_col = GetComponent<SphereCollider>();

        boom_col.enabled = false;
    }

    public void Update()
    {
        if (!boom_activate)
            transform.Translate(Vector3.down * speed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "shield")
        {
            if (!boom_activate)
            {
                boom_vfx.Play();
                mesh.SetActive(false);
                boom_activate = true;
                gameObject.GetComponent<BoxCollider>().enabled = false;
                return;
            }
        }

        if (other.tag == "ground")
        {
            if (!boom_activate)
            {
                boom_col.enabled = true;
                boom_vfx.Play();
                mesh.SetActive(false);
                boom_activate = true;

                StartCoroutine(OffCol());
            }
        }

        if (other.tag == "Enemy" && other.gameObject.GetComponentInParent<PetBrain>().team != brain.team)
        {
            other.gameObject.GetComponentInParent<PetBrain>().StartCoroutine(other.gameObject.GetComponentInParent<PetBrain>().HitEnum());
            other.gameObject.GetComponentInParent<PetBrain>().Hit(father, brain.dmg);
            other.gameObject.GetComponentInParent<PetBrain>().curr_hp -= brain.dmg * 3;
        }
    }

    IEnumerator OffCol()
    {
        yield return new WaitForSeconds(0.5f);
        boom_col.enabled = false;
    }
}
