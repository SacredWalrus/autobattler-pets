using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetKnifeCollider : MonoBehaviour
{
    public PetBrain brain;
    public GameObject father;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "shield")
        {
            gameObject.SetActive(false);
            return;
        }

        if (other.tag == "Enemy" && other.gameObject.GetComponentInParent<PetBrain>().team != brain.team)
        {
            other.gameObject.GetComponentInParent<PetBrain>().StartCoroutine(other.gameObject.GetComponentInParent<PetBrain>().HitEnum());
            other.gameObject.GetComponentInParent<PetBrain>().Hit(father, brain.dmg);
            other.gameObject.GetComponentInParent<PetBrain>().curr_hp -= brain.dmg;
        }
    }
}
