using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetKnifeController : MonoBehaviour
{
    [Header("��������� ��� ���������")]
    public float min_attack_dist;
    public float max_dist_to_enemy_move_back;  //������������ ��������� �� ����� ��� �����������
    public float attack_cd;

    [Header("��������� ��� ������")]
    public Animator anim;

    PetBrain brain;

    public enum State
    {
        none,
        RoundStart,
        MoveToTarget,
        MoveBack,
        Die,
        Attack,
        Ulta,
        WalkToSelection
    }
    public State curr_state;
    public Vector3 selection_point;


    public GameObject attack_collider;

    private float move_back_timer;

    public GameObject target_knife_obj, knife_ulta;

    public ParticleSystem vfx_ulta;
    public GameObject ulta_collider;


    private void Awake()
    {
        curr_state = State.RoundStart;

        brain = GetComponent<PetBrain>();
    }

    IEnumerator Ulta()
    {
        vfx_ulta.Play();

        ulta_collider.SetActive(true);

        yield return new WaitForSeconds(0.2f);

        ulta_collider.SetActive(false);

        yield return new WaitForSeconds(0.2f);

        ulta_collider.SetActive(true);

        yield return new WaitForSeconds(0.2f);

        ulta_collider.SetActive(false);

        yield return new WaitForSeconds(0.2f);

        ulta_collider.SetActive(true);

        yield return new WaitForSeconds(0.2f);

        ulta_collider.SetActive(false);

        yield return new WaitForSeconds(0.2f);

        ulta_collider.SetActive(true);

        yield return new WaitForSeconds(0.2f);

        ulta_collider.SetActive(false);

        curr_state = State.none;
        vfx_ulta.Stop();
    }

    public void StopAction()
    {
        StopAllCoroutines();
    }

    private void Update()
    {
        if (curr_state == State.WalkToSelection)
        {
            brain.Movement(selection_point);

            //LookAt
            Vector3 lookPos = selection_point - transform.position;
            lookPos.y = 0;
            Quaternion rotation = Quaternion.LookRotation(lookPos);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, brain.rotation_speed);

            if (brain.DistToTarget(selection_point) <= 1)
            {
                curr_state = State.none;
            }
        }

        if (brain.ulta)
        {
            StopAllCoroutines();
            curr_state = State.Ulta;
            StartCoroutine(Ulta());

            brain.ulta = false;
        }

        if (curr_state == State.Ulta)
        {
            transform.Rotate(new Vector3(0, 40, 0));
        }

        if (brain.round_start && curr_state == State.RoundStart)
        {
            curr_state = State.none;
            brain.round_start = false;
        }

        if (curr_state == State.none)
        {
            if (brain.target != null)
                ChangeState();
        }
        
        if (curr_state == State.MoveToTarget)
        {            
            brain.Movement(brain.target.transform.position);

            //LookAt
            Vector3 lookPos = brain.target.transform.position - transform.position;
            lookPos.y = 0;
            Quaternion rotation = Quaternion.LookRotation(lookPos);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, brain.rotation_speed);

            if (brain.DistToTarget(brain.target.transform.position) <= min_attack_dist)
            {
                curr_state = State.Attack;
                StartCoroutine(Attack());
            }
        }

        anim.SetFloat("speed", brain.rb.velocity.magnitude);

        if (brain.curr_hp <= 0 && curr_state != State.Die)
        {
            StopAllCoroutines();
            ulta_collider.SetActive(false);
            vfx_ulta.Stop();
            StartCoroutine(Die());
        }

        if (GameObject.Find("GameController").GetComponent<FightController>().curr_round_time <= 0)
        {
            StopAllCoroutines();
            ulta_collider.SetActive(false);
            vfx_ulta.Stop();
            curr_state = State.RoundStart;
        }
    }

    IEnumerator Die()
    {
        brain.die = true;
        attack_collider.SetActive(false);
        brain.canvas_ui.SetActive(false);
        brain.stan = false;

        GameObject.Find("GameController").GetComponent<FightController>().pets_in_arena.Remove(gameObject);
        anim.SetTrigger("die");
        curr_state = State.Die;

        if (brain.team == PetBrain.Team.team_1)
        {
            GameObject.Find("GameController").GetComponent<FightController>().team_2_kill++;
        }

        if (brain.team == PetBrain.Team.team_2)
        {
            GameObject.Find("GameController").GetComponent<FightController>().team_1_kill++;
        }

        yield return new WaitForSeconds(3);

        brain.Respawn();
        GameObject.Find("GameController").GetComponent<FightController>().pets_in_arena.Add(gameObject);
        anim.SetTrigger("respawn");
        curr_state = State.none;
        brain.target = null;
        brain.canvas_ui.SetActive(true);
        brain.die = false;
    }

    void ChangeState()
    {
        if (brain.DistToTarget(brain.target.transform.position) <= min_attack_dist + 0.5f && brain.DistToTarget(brain.target.transform.position) >= min_attack_dist - 0.5 && !brain.stan)
        {
            curr_state = State.Attack;
            StartCoroutine(Attack());
            return;
        }

        if (brain.DistToTarget(brain.target.transform.position) > min_attack_dist - 0.5f)
        {
            curr_state = State.MoveToTarget;
        }

        if (brain.DistToTarget(brain.target.transform.position) < min_attack_dist + 0.5f)
        {
            curr_state = State.MoveBack;
            Vector3 target_pos = brain.target.transform.position;

            move_back_timer = 0.2f;
            StartCoroutine(MoveBack(target_pos, move_back_timer));
            return;
        }        
    }

    IEnumerator Attack()
    {
        anim.SetTrigger("attack");

        //LookAt
        Vector3 lookPos = brain.target.transform.position - transform.position;
        lookPos.y = 0;
        Quaternion rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, brain.rotation_speed);

        yield return new WaitForSeconds(0.1f);

        attack_collider.SetActive(true);

        yield return new WaitForSeconds(attack_cd - 0.2f);

        attack_collider.SetActive(false);

        yield return new WaitForSeconds(0.1f);


        curr_state = State.none;
    }

    IEnumerator MoveBack(Vector3 target_pos, float timer)
    {
        yield return new WaitForSeconds(0);
        brain.MovementBack(target_pos);

        timer -= Time.deltaTime;

        if (timer > 0)
        {
            StartCoroutine(MoveBack(target_pos, timer));
        }
        else
        {
            curr_state = State.none;
        }
    }
}
