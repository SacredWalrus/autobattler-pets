using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetKnifeUlta : MonoBehaviour
{
    public GameObject target;
    public float speed;

    public PetBrain brain;
    public GameObject father;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy" && other.gameObject.GetComponentInParent<PetBrain>().team != brain.team)
        {
            StartCoroutine(other.gameObject.GetComponentInParent<PetBrain>().HitEnum());
            other.gameObject.GetComponentInParent<PetBrain>().Hit(father, brain.dmg);
            other.gameObject.GetComponentInParent<PetBrain>().curr_hp -= brain.dmg;
            Destroy(gameObject);
        }
    }

    public void Start()
    {
        Destroy(gameObject, 10);
    }

    public void Update()
    {
        //LookAt
        Vector3 lookPos = target.transform.position - transform.position;
        //lookPos.y = 0;
        Quaternion rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 1);

        transform.Translate(transform.forward * speed * Time.deltaTime);
    }
}
