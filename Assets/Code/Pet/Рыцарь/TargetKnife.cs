using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetKnife : MonoBehaviour
{
    public GameObject target;

    public void Start()
    {
        Destroy(gameObject, 3);
    }

    public void Update()
    {
        Vector3 pos = new Vector3(target.transform.position.x, 1.49f, target.transform.position.z - 1.16f);

        transform.position = pos;

        transform.localEulerAngles = new Vector3(0, 90, -47);
    }
}
