using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    public float speed;

    public PetBrain brain;
    public GameObject father;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy" && other.gameObject.GetComponentInParent<PetBrain>().team != brain.team)
        {
            other.gameObject.GetComponentInParent<PetBrain>().StartCoroutine(other.gameObject.GetComponentInParent<PetBrain>().HitEnum());
            other.gameObject.GetComponentInParent<PetBrain>().Hit(father, brain.dmg);
            other.gameObject.GetComponentInParent<PetBrain>().curr_hp -= brain.dmg;
            Destroy(gameObject);
        }
    }

    public void Start()
    {
        Destroy(gameObject, 10);
    }

    public void Update()
    {        
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }
}
