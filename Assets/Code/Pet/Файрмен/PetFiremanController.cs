using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetFiremanController : MonoBehaviour
{
    [Header("��������� ��� ���������")]
    public float min_attack_dist;
    public float max_dist_to_enemy_move_back;  //������������ ��������� �� ����� ��� �����������
    public float attack_cd;

    [Header("��������� ��� ������")]
    public Animator anim;

    PetBrain brain;

    public enum State
    {
        none,
        RoundStart,
        MoveToTarget,
        MoveBack,
        Die,
        Attack,
        Walk,
        Ulta,
        WalkToSelection
    }
    public State curr_state;
    public Vector3 selection_point;


    private float move_back_timer;

    public GameObject fire_obj;
    public GameObject fire_shield;

    bool attack_access = true;
    bool attack_active = false;
    bool moving_back = false;

    Vector3 walk_pos;


    private void Awake()
    {
        curr_state = State.RoundStart;

        brain = GetComponent<PetBrain>();
    }

    public void StopAction()
    {
        StopAllCoroutines();
        moving_back = false;
        attack_active = false;
        attack_access = true;
    }

    private void Update()
    {
        if (curr_state == State.WalkToSelection)
        {
            brain.Movement(selection_point);

            //LookAt
            Vector3 lookPos = selection_point - transform.position;
            lookPos.y = 0;
            Quaternion rotation = Quaternion.LookRotation(lookPos);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, brain.rotation_speed);

            if (brain.DistToTarget(selection_point) <= 1)
            {
                curr_state = State.none;
            }
        }

        if (brain.round_start && curr_state == State.RoundStart)
        {
            curr_state = State.none;
            brain.round_start = false;
        }

        if (curr_state == State.Walk)
        {
            brain.Movement(walk_pos);
            if (Vector3.Distance(transform.position, walk_pos) < 0.5f)
            {
                curr_state = State.none;
            }
        }

        if (curr_state == State.none)
        {
            if (brain.target != null)
                ChangeState();
        }

        if (curr_state == State.MoveToTarget)
        {
            brain.Movement(brain.target.transform.position);

            //LookAt
            Vector3 lookPos = brain.target.transform.position - transform.position;
            lookPos.y = 0;
            Quaternion rotation = Quaternion.LookRotation(lookPos);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, brain.rotation_speed);

            if (brain.DistToTarget(brain.target.transform.position) <= min_attack_dist)
            {
                curr_state = State.Attack;
                StartCoroutine(Attack());
            }
        }

        anim.SetFloat("speed", brain.rb.velocity.magnitude);

        if (brain.curr_hp <= 0 && curr_state != State.Die)
        {
            StopAllCoroutines();
            StartCoroutine(Die());
        }

        if (GameObject.Find("GameController").GetComponent<FightController>().curr_round_time <= 0)
        {
            StopAllCoroutines();
            curr_state = State.RoundStart;
        }

        if (brain.ulta)
        {
            curr_state = State.Ulta;
            StartCoroutine(Ulta());
            brain.ulta = false;
        }
    }

    IEnumerator Ulta()
    {
        foreach (GameObject gm in GameObject.Find("GameController").GetComponent<FightController>().pets_in_arena)
        {
            if (gm.GetComponent<PetBrain>().team == brain.team)
            {
                GameObject g = Instantiate(fire_shield, gm.transform.position, transform.rotation);
                g.GetComponent<FireShield>().brain = gm.GetComponent<PetBrain>();
                g.GetComponent<FireShield>().father = gm;
            }
        }

        yield return new WaitForSeconds(attack_cd);

        bool accept = false;
        FightController fight_control = GameObject.Find("GameController").GetComponent<FightController>();

        while (!accept)
        {
            walk_pos = new Vector3(Random.Range(transform.position.x - 2.5f, transform.position.x + 2.5f),
                                   transform.position.y,
                                   Random.Range(transform.position.z - 2.5f, transform.position.z + 2.5f));

            if (walk_pos.x < fight_control.max_border_x &&
                walk_pos.x > fight_control.min_border_x &&
                walk_pos.z < fight_control.max_border_z &&
                walk_pos.z > fight_control.min_border_z)
            {
                accept = true;
            }
        }
        curr_state = State.Walk;
        attack_active = false;
        anim.SetTrigger("attack_end");
    }

    IEnumerator Die()
    {
        brain.die = true;
        brain.canvas_ui.SetActive(false);
        brain.stan = false;

        GameObject.Find("GameController").GetComponent<FightController>().pets_in_arena.Remove(gameObject);
        anim.SetTrigger("die");
        curr_state = State.Die;

        if (brain.team == PetBrain.Team.team_1)
        {
            GameObject.Find("GameController").GetComponent<FightController>().team_2_kill++;
        }

        if (brain.team == PetBrain.Team.team_2)
        {
            GameObject.Find("GameController").GetComponent<FightController>().team_1_kill++;
        }

        yield return new WaitForSeconds(3);

        moving_back = false;
        attack_active = false;
        attack_access = true;
        brain.Respawn();
        GameObject.Find("GameController").GetComponent<FightController>().pets_in_arena.Add(gameObject);
        anim.SetTrigger("respawn");
        curr_state = State.none;
        brain.target = null;
        brain.canvas_ui.SetActive(true);
        brain.die = false;
    }

    void ChangeState()
    {
        if (brain.DistToTarget(brain.target.transform.position) <= min_attack_dist)
        {
            if (attack_access && !brain.stan)
            {
                moving_back = false;
                curr_state = State.Attack;
                StartCoroutine(Attack());
            }
        }

        if (!attack_active && !moving_back)
        {
            //LookAt
            Vector3 lookPos = brain.target.transform.position - transform.position;
            lookPos.y = 0;
            Quaternion rotation = Quaternion.LookRotation(lookPos);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, brain.rotation_speed);

            foreach (GameObject gm in GameObject.Find("GameController").GetComponent<FightController>().pets_in_arena)
            {
                if (gm.GetComponent<PetBrain>().team != brain.team)
                {
                    if (brain.DistToTarget(gm.transform.position) < max_dist_to_enemy_move_back)
                    {
                        moving_back = true;
                        curr_state = State.MoveBack;
                        float timer = Random.Range(1f, 3f);
                        StartCoroutine(MoveBack(gm, timer));
                        return;
                    }
                }
            }
        }
    }

    IEnumerator Attack()
    {
        attack_active = true;
        anim.SetTrigger("attack");
        attack_access = false;

        //LookAt
        Vector3 lookPos = brain.target.transform.position - transform.position;
        lookPos.y = 0;
        Quaternion rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 1);

        yield return new WaitForSeconds(0.1f);

        GameObject gm = Instantiate(fire_obj, new Vector3(transform.position.x, transform.position.y, transform.position.z + 1), transform.rotation);
        gm.GetComponent<Fireball>().brain = brain;
        gm.GetComponent<Fireball>().father = gameObject;

        yield return new WaitForSeconds(0.4f);

        StartCoroutine(AttackCD());
        bool accept = false;
        FightController fight_control = GameObject.Find("GameController").GetComponent<FightController>();

        while (!accept)
        {
            walk_pos = new Vector3(Random.Range(transform.position.x - 2.5f, transform.position.x + 2.5f),
                                   transform.position.y,
                                   Random.Range(transform.position.z - 2.5f, transform.position.z + 2.5f));

            if (walk_pos.x < fight_control.max_border_x &&
                walk_pos.x > fight_control.min_border_x &&
                walk_pos.z < fight_control.max_border_z &&
                walk_pos.z > fight_control.min_border_z)
            {
                accept = true;
            }
        }
        curr_state = State.Walk;
        attack_active = false;
    }

    IEnumerator AttackCD()
    {
        yield return new WaitForSeconds(attack_cd);
        attack_access = true;
    }

    IEnumerator MoveBack(GameObject gm, float timer)
    {
        yield return new WaitForSeconds(0);
        timer -= Time.deltaTime;

        bool isDirSafe = false;

        //We will need to rotate the direction away from the player if straight to the opposite of the player is a wall
        float vRotation = 0;


        while (!isDirSafe)
        {
            Vector3 dir_to_enemy = transform.position - gm.transform.position;
            Vector3 newPos = transform.position + dir_to_enemy;

            //Rotate the direction of the Enemy to move
            newPos = Quaternion.Euler(0, vRotation, 0) * newPos;

            //Shoot a Raycast out to the new direction with 5f length (as example raycast length) and see if it hits an obstacle
            bool isHit = Physics.Raycast(transform.position, newPos, out RaycastHit hit, 3f);

            if (hit.transform == null)
            {
                //If the Raycast to the flee direction doesn't hit a wall then the Enemy is good to go to this direction
                brain.Movement(newPos);
                isDirSafe = true;
            }

            //Change the direction of fleeing is it hits a wall by 20 degrees
            if (isHit && hit.transform.CompareTag("Wall"))
            {
                vRotation += 20;
                isDirSafe = false;
            }
            else
            {
                //If the Raycast to the flee direction doesn't hit a wall then the Enemy is good to go to this direction
                brain.Movement(newPos);
                isDirSafe = true;
            }
        }

        if (timer > 0)
        {
            StartCoroutine(MoveBack(gm, timer));
        }
        else
        {
            moving_back = false;
            curr_state = State.none;
        }
    }
}
