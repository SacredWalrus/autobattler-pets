using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireShield : MonoBehaviour
{
    public PetBrain brain;
    public GameObject father;
    public float speed;

    private bool attack_accept;

    IEnumerator Attack()
    {
        yield return new WaitForSeconds(3);
        attack_accept = true;

        
    }

    private void Start()
    {
        attack_accept = false;
        StartCoroutine(Attack());
        Destroy(gameObject, 10);
    }

    private void Update()
    {
        if (attack_accept)
        {
            //LookAt
            Vector3 lookPos = brain.target.transform.position - transform.position;
            //lookPos.y = 0;
            Quaternion rotation = Quaternion.LookRotation(lookPos);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 1);

            transform.Translate(Vector3.forward * speed * Time.deltaTime);
            if (transform.localScale.x > 0.2f)
            {
                transform.localScale = new Vector3(transform.localScale.x - Time.deltaTime * 5,
                                                   transform.localScale.y - Time.deltaTime * 5,
                                                   transform.localScale.z - Time.deltaTime * 5);
            }
        }
        else
        {
            transform.position = father.transform.position;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (attack_accept)
        {
            if (other.tag == "Enemy" && other.gameObject.GetComponentInParent<PetBrain>().team != brain.team)
            {
                other.gameObject.GetComponentInParent<PetBrain>().StartCoroutine(other.gameObject.GetComponentInParent<PetBrain>().HitEnum());
                other.gameObject.GetComponentInParent<PetBrain>().Hit(father, brain.dmg);
                other.gameObject.GetComponentInParent<PetBrain>().curr_hp -= brain.dmg;
                Destroy(gameObject);
            }
        }
    }
}
